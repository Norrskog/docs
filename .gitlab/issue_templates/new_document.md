# Issue: Chronologue document

The main goal of the Chronologue project is to write examples of good documentation by testing templates created by the templates team. Use this issue to create a Chronologue example document for the {template name} template.
Link to template in the repository: [Template name](https://gitlab.com/tgdp/templates)

## Link to Google Doc drafts

To keep ownership and manage an archive of the drafts, The Good Docs Project has pre-generated Google doc files for you to test templates and document your process.

- [ ] Chronologue example document - [{Content type} example](link-to-Google-Doc).

- [ ] Decision log - [{Content type} decisions](link-to-Google-Doc). 

- [ ] Friction log - [{Content type} friction](link-to-Google-Doc). 

Bookmark these links and use them during your writing process. You can always consult the links in this issue. 


To know more about the drafting process, please read the [Cronologue Contributing Guide](https://gitlab.com/tgdp/chronologue/docs/-/blob/main/CONTRIBUTING.md?ref_type=heads#overview-of-the-chronologue-documentation-process).

## Definition of done

We consider a document finished when it has gone through all the writing stages and it has (at least) a Chronologue example document and a [link to it in the {template name} repository](https://gitlab.com/tgdp/chronologue/docs/-/blob/main/CONTRIBUTING.md?ref_type=heads#deliverables). 

### Writing stages

- [ ] **Research stage** (optional). For any Chronologue-building decisions your documentation requires, use the _Decision log_ file to document these decisions and any resources you might use. For example, to write an installation guide, you may need to invent a third-party app that allows you to view astronomic Chronologue recordings in a VR headset. You would use the Decision log file to document the creation process of this new feature. 
- [ ] **Drafting stage**. Test your template and draft your document in the _Chronologue example document_. During this stage you can also document the friction points you find when using the template in the _Friction log_ file. 
- [ ] **Community review stage**. Communicate to your working group leaders when your draft is ready for review by the team. They will schedule a session where team members will read and provide feedback to your document. 
- [ ] **Template feedback** (optional). If you have any feedback to improve the template you tested, open an issue directly in the templates reporsitory. You can upload the _Friction log_ file. Make sure to give actionable feedback in the issue. 
- [ ] **Final review stage**. After the team has reviewed your draft at least one time, you can convert your Google doc into a Markdown file and open a merge request in the [Documentation repository](https://gitlab.com/tgdp/chronologue/docs/-/tree/main/content/en/docs?ref_type=heads). Chronologue leads may ask for additional changes. After the group leads approve your document, you can merge it.  


### Chronologue deliverables

- [ ] Chronologue example document.
- [ ] [Link to Chronologue example](https://gitlab.com/tgdp/chronologue/docs/-/blob/main/CONTRIBUTING.md?ref_type=heads#deliverables) in the {template name} repo. 
- [ ] Decision log file (if applicable).
- [ ] Issue in templates repository to provide feedback (if applicable). 


## Working on this issue

Check that the issue is unassigned. If it is assigned, you might be able to work with the current assignee as a paired writer. Follow these requirements to work on this issue:

1. Join The Good Docs Project by attending a [Welcome Wagon meeting](https://thegooddocsproject.dev/welcome/) and ask the community managers to invite you to The Good Docs Project Slack workspace. 
2. Read the [Chronologue Contributing Guide](https://gitlab.com/tgdp/chronologue/docs/-/blob/main/CONTRIBUTING.md?ref_type=heads#overview-of-the-chronologue-documentation-process).
4. Attend one of our [Chronologue working groups](https://thegooddocsproject.dev/community/#calendar) regularly to receive support, mentorship, resources, and feedback. 

