---
title: Overview of the recording process
draft: true
menu:
  docs:
    parent: "docs"
    identifier: "Recording process"
weight: 30
---


<!--This document explains the steps of the recording process: 
1. Requesting a new recording (+ link to the task topic)
2. Chronologue committee reviews the request, and makes alterations if needed. We could discuss acceptance/rejection criteria here.
3. Requesting the recording from the API (links to API documentation) -->
